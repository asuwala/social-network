package com.adriansuwala.uj.socnet.util;

import java.time.Instant;

public interface TimeProvider {
    static Instant getTime() {
        return Instant.now();
    }
}
