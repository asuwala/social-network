package com.adriansuwala.uj.socnet.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "Notifications")
@Data
public class Notification {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NotificationIdSequence")
    @SequenceGenerator(name = "NotificationIdSequence", allocationSize = 1)
    private Integer id;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "receiver", referencedColumnName = "id", nullable = false)
    private User receiver;

    @Column(name = "sendTime", columnDefinition = "datetime")
    private Instant sendTime;

    @Column(name = "readTime", columnDefinition = "datetime")
    private Instant readTime;

    @Column(name = "message", columnDefinition = "ntext")
    private String message;

    public Notification(User receiver, Instant sendTime, String message) {
        this.receiver = receiver;
        this.sendTime = sendTime;
        this.message = message;
    }
}
