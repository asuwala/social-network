package com.adriansuwala.uj.socnet.entities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.Instant;

@Projection(name = "PostProjection", types = {Post.class })
public interface PostProjection {
    Integer getId();

    Instant getCreationTime();

    @Value("#{target.author.id}")
    String getAuthorId();

    @Value("#{target.author.firstName}")
    String getAuthorFirstName();

    @Value("#{target.author.lastName}")
    String getAuthorLastName();

    String getBody();

    String getVisibility();

    @Value("#{target.reactions.size()}")
    String getReactions();

    String getComments();
}
