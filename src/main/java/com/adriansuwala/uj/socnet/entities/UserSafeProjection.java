package com.adriansuwala.uj.socnet.entities;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "limited", types = {User.class })
public interface UserSafeProjection {
    Integer getId();

    String getEmail();

    String getFirstName();

    String getLastName();

    String getProfilePicture();
}
