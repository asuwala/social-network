package com.adriansuwala.uj.socnet.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "Posts")
@Data
@NoArgsConstructor
@ToString(callSuper = true, of = {"visibility", "group", "comments"})
public class Post extends Element {
    @JsonIgnore
    @Column(columnDefinition = "nvarchar", length = 1)
    private String visibility;

    @ManyToOne(targetEntity = Group.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "groupId", referencedColumnName = "id")
    private Group group;

    @Column(columnDefinition = "ntext")
    private String tags;

    @OneToMany(targetEntity = Comment.class, mappedBy = "post")
    List<Comment> comments;

    public Post(String body, User author, String visibility, Group group, Instant creationTime) {
        this.setBody(body);
        this.setAuthor(author);
        this.setElementType("POST");
        this.setCreationTime(creationTime);
        this.visibility = visibility;
        this.group = group;
    }
}
