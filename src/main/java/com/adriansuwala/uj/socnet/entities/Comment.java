package com.adriansuwala.uj.socnet.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "Comments")
@NoArgsConstructor
@PrimaryKeyJoinColumn(name = "id")
@SecondaryTable(name = "CountSubcomments", pkJoinColumns = @PrimaryKeyJoinColumn(name = "commentId", referencedColumnName = "id"))
public class Comment extends Element {
    @ManyToOne(targetEntity = Comment.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "parentCommentId")
    private Comment parentComment;

    @JsonIgnore
    @ManyToOne(targetEntity = Post.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "postId", nullable = false)
    private Post post;

    @Column(table = "CountSubcomments", name = "commentCount", insertable = false, updatable = false)
    private Integer commentCount = 0;

    @JsonIgnore
    @Column(columnDefinition = "bit", nullable = false)
    private boolean isRemoved = false;

    public Comment(String body, User author, Instant creationTime, Comment parentComment, Post post) {
        this.setBody(body);
        this.setAuthor(author);
        this.setElementType("POST");
        this.setCreationTime(creationTime);
        this.parentComment = parentComment;
        this.post = post;
    }
}
