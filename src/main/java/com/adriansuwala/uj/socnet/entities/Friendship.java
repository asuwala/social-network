package com.adriansuwala.uj.socnet.entities;

import com.adriansuwala.uj.socnet.data.events.FriendRequestAcceptedEvent;
import com.adriansuwala.uj.socnet.data.events.FriendRequestRejectedEvent;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.Transient;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "Friendships")
@Data
@NoArgsConstructor
public class Friendship {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FriendshipIdSequence")
    @SequenceGenerator(name = "FriendshipIdSequence", allocationSize = 1)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "senderId", referencedColumnName = "id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "receiverId", referencedColumnName = "id")
    private User receiver;

    @Column(name = "friendshipStart", columnDefinition = "datetime", nullable = false)
    private Instant friendshipStart;

    @Column(name = "status", columnDefinition = "nvarchar", length = 1, nullable = false)
    private String status = "P";

    public Friendship(User sender, User receiver, Instant friendshipStart, String status) {
        this.sender = sender;
        this.receiver = receiver;
        this.friendshipStart = friendshipStart;
        this.status = status;
    }

    @Transient
    public enum FriendshipStatus {
        PENDING {
            @Override
            public ApplicationEvent makeEvent(Friendship friendship) {
                throw new IllegalStateException("no event for pending request");
            }
        },
        ACCEPTED {
            @Override
            public ApplicationEvent makeEvent(Friendship friendship) {
                return new FriendRequestAcceptedEvent(friendship);
            }
        },
        REJECTED {
            @Override
            public ApplicationEvent makeEvent(Friendship friendship) {
                return new FriendRequestRejectedEvent(friendship);
            }
        };

        public abstract ApplicationEvent makeEvent(Friendship friendship);

        public static FriendshipStatus fromString(String s) {
            switch (s) {
                case "A":
                    return FriendshipStatus.ACCEPTED;
                case "R":
                    return FriendshipStatus.REJECTED;
                case "P":
                    return FriendshipStatus.PENDING;
                default:
                    throw new IllegalStateException("Unexpected value: " + s);
            }
        }
    }
}
