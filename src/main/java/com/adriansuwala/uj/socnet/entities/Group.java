package com.adriansuwala.uj.socnet.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;

@Entity
@Table(name = "Groups")
@Data
public class Group {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", columnDefinition = "nvarchar", length = 20)
    private String name;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @Column(name = "creationTime", columnDefinition = "datetime")
    private Instant creationTime;

    @Column(name = "visibility", columnDefinition = "nvarchar", length = 1)
    private String visibility;

    @ManyToMany(targetEntity = User.class)
    @JoinTable(name = "UsersGroups",
            joinColumns = @JoinColumn(name = "groupId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "userId", referencedColumnName = "id")
    )
    private Set<User> users;
}
