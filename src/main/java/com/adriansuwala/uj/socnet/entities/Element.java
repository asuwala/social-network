package com.adriansuwala.uj.socnet.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Data
@Table(name = "Elements")
@Inheritance(strategy = InheritanceType.JOINED)
@SecondaryTable(name = "CountElementReactions", pkJoinColumns = @PrimaryKeyJoinColumn(name = "elementId", referencedColumnName = "id"))
public abstract class Element {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ElementsIdSequence")
    @SequenceGenerator(name = "ElementsIdSequence", allocationSize = 1)
    private Integer id;

    @Column(columnDefinition = "datetime", nullable = false)
    private Instant creationTime = Instant.now();

    @Column(columnDefinition = "ntext", nullable = false)
    private String body;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "authorId", nullable = false)
    private User author;

    @JsonIgnore
    @Column(columnDefinition = "varchar", length = 8)
    private String elementType;

    @Column(table = "CountElementReactions", name = "reactionCount", insertable = false, updatable = false)
    @JoinColumn(name = "elementId", referencedColumnName = "id", table = "CountElementReactions")
    private Integer reactionCount = 0;
}
