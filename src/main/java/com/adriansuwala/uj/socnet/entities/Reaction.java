package com.adriansuwala.uj.socnet.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "UsersElementsReactions")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reaction {

    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class ReactionId implements Serializable {
        private Integer userId;
        private Integer elementId;
    }

    public static Reaction from(Integer userId, Integer elementId) {
        return new Reaction(new ReactionId(userId, elementId));
    }

    @EmbeddedId
    private ReactionId id;
}
