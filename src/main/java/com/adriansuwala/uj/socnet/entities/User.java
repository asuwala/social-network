package com.adriansuwala.uj.socnet.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.sql.Date;
import java.time.Instant;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "Users", schema = "socnet")
@Getter
@Setter
@EqualsAndHashCode(of = {"id", "email"})
@ToString(exclude = "groups")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UsersIdSequence")
    @SequenceGenerator(name = "UsersIdSequence", allocationSize = 1)
    private Integer id;

    @JsonIgnore
    @Column(columnDefinition = "nvarchar", length = 320, nullable = false)
    private String email;

    @JsonIgnore
    @Column(columnDefinition = "nvarchar", length = 256, nullable = false)
    private String password;

    @Column(columnDefinition = "nvarchar", length = 100, nullable = false)
    private String firstName;

    @Column(columnDefinition = "nvarchar", length = 100, nullable = false)
    private String lastName;

    @JsonIgnore
    @Column(columnDefinition = "datetime", nullable = false)
    private Instant registrationTime;

    @Column(columnDefinition = "nvarchar", length = 50)
    private String profilePicture;

    @JsonIgnore
    @Column(columnDefinition = "datetime")
    private Date birthday;

    @JsonIgnore
    @Column(columnDefinition = "nvarchar", length = 10)
    private String gender;

    @JsonIgnore
    @Column(columnDefinition = "nvarchar", length = 50)
    private String occupation;

    @JsonIgnore
    @Transient
    private Collection<? extends GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority("USER"));

    private User(Integer id, String email, String password, String firstName, String lastName, Instant registrationTime, String profilePicture, Date birthday, String gender, String occupation) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.registrationTime = registrationTime;
        this.profilePicture = profilePicture;
        this.birthday = birthday;
        this.gender = gender;
        this.occupation = occupation;
    }

    public User() {
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    @JsonIgnore
    @Transient
    @Override
    public String getUsername() {
        return email;
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Transient
    @Override
    public boolean isEnabled() {
        return true;
    }

    public static class UserBuilder {
        private Integer id;
        private String email;
        private String password;
        private String firstName;
        private String lastName;
        private Instant registrationTime;
        private String profilePicture;
        private Date birthday;
        private String gender;
        private String occupation;

        UserBuilder() {
        }

        public UserBuilder id(Integer id) {
            this.id = id;
            return this;
        }

        public UserBuilder email(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder password(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder registrationTime(Instant registrationTime) {
            this.registrationTime = registrationTime;
            return this;
        }

        public UserBuilder profilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
            return this;
        }

        public UserBuilder birthday(Date birthday) {
            this.birthday = birthday;
            return this;
        }

        public UserBuilder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public UserBuilder occupation(String occupation) {
            this.occupation = occupation;
            return this;
        }

        public User build() {
            return new User(id, email, password, firstName, lastName, registrationTime, profilePicture, birthday, gender, occupation);
        }

        public String toString() {
            return "User.UserBuilder(id=" + this.id + ", email=" + this.email + ", password=" + this.password + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", registrationTime=" + this.registrationTime + ", profilePicture=" + this.profilePicture + ", birthday=" + this.birthday + ", gender=" + this.gender + ", occupation=" + this.occupation + ")";
        }
    }
}
