package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.entities.Notification;
import com.adriansuwala.uj.socnet.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, String> {

    Page<Notification> findNotificationsByReceiverOrderBySendTimeDesc(User receiver, Pageable pageable);

    Page<Notification> findNotificationsByReceiverAndReadTimeIsNullOrderBySendTimeDesc(User receiver, Pageable pageable);
}
