package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.data.PostTrackerData;
import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostEngagementDataRepository extends KeyValueRepository<PostTrackerData, Integer> {

    List<PostTrackerData> findAllByPostId(Integer postId);

}
