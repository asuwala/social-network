package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.entities.Reaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface ReactionRepository extends JpaRepository<Reaction, Reaction.ReactionId> {
}
