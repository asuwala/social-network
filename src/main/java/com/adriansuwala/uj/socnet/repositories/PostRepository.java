package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.time.Instant;

@RepositoryRestResource(collectionResourceRel = "posts", path = "posts")
public interface PostRepository extends JpaRepository<Post, Integer> {

    @Query(value = "SELECT p FROM Post p JOIN FETCH p.comments JOIN p.author WHERE p.visibility = :visibility ORDER BY p.creationTime DESC",
            countQuery = "SELECT Count(p) FROM Post p WHERE p.visibility = :visibility")
    Page<Post> findAllByVisibilityOrderByCreationTimeDesc(@Param("visibility") String visibility, Pageable pageable);

    @Query(value = "SELECT p FROM Post p JOIN FETCH p.comments JOIN p.author WHERE p.creationTime >= :threshold AND " +
            "(p.visibility = 'P' OR " +
            "(p.visibility = 'F' AND p.author IN " +
            "(SELECT u FROM User u WHERE u.id IN " +
            "(SELECT CASE WHEN f.sender = :user THEN f.receiver ELSE f.sender END FROM Friendship f WHERE f.status = 'A' AND (f.sender = :user OR f.receiver = :user)))))",
            countQuery = "SELECT Count(p) FROM Post p WHERE p.creationTime >= :threshold AND " +
                    "(p.visibility = 'P' OR " +
                    "(p.visibility = 'F' AND p.author IN " +
                    "(SELECT u FROM User u WHERE u.id IN " +
                    "(SELECT CASE WHEN f.sender = :user THEN f.receiver ELSE f.sender END FROM Friendship f WHERE f.status = 'A' AND (f.sender = :user OR f.receiver = :user)))))")
    Page<Post> findAllVisiblePostsAfter(@Param("user") User user, @Param("threshold") Instant threshold, Pageable pageable);
}
