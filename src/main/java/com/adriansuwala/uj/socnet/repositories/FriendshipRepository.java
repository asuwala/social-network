package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.entities.Friendship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false, path = "friendships")
public interface FriendshipRepository extends JpaRepository<Friendship, Integer> {
}
