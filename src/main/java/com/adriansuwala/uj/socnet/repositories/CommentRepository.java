package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "comments", path = "comments")
public interface CommentRepository extends JpaRepository<Comment, Integer> {

//    List<Comment> findTop10ByPostIsInOrderByElement_CreationTimeAsc(List<Post> posts);

//    Set<Comment> findByPost(Post post);
}
