package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(@Param("email") String email);

    @Query(value = "SELECT u FROM User u WHERE u.id IN " +
            "(SELECT CASE WHEN f.sender = :user THEN f.receiver ELSE f.sender END " +
            "FROM Friendship f WHERE f.status = 'A' AND (f.sender = :user OR f.receiver = :user))")
    Page<User> findAllFriends(@Param("user") User id, Pageable pageable);
}
