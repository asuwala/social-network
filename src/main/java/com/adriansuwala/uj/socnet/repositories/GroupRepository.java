package com.adriansuwala.uj.socnet.repositories;

import com.adriansuwala.uj.socnet.entities.Group;
import com.adriansuwala.uj.socnet.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface GroupRepository extends JpaRepository<Group, Integer> {

    Set<Group> findGroupsByUsersContains(User user);
}
