package com.adriansuwala.uj.socnet.services;

import com.adriansuwala.uj.socnet.components.TimelineGenerator;
import com.adriansuwala.uj.socnet.data.events.*;
import com.adriansuwala.uj.socnet.entities.*;
import com.adriansuwala.uj.socnet.repositories.FriendshipRepository;
import com.adriansuwala.uj.socnet.repositories.GroupRepository;
import com.adriansuwala.uj.socnet.repositories.NotificationRepository;
import com.adriansuwala.uj.socnet.repositories.UserRepository;
import com.adriansuwala.uj.socnet.util.TimeProvider;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final NotificationRepository notificationRepository;
    private final GroupRepository groupRepository;
    @Qualifier("mostRecentTimelineGenerator")
    private final TimelineGenerator timelineGenerator;
    private final UserRepository userRepository;
    private final FriendshipRepository friendshipRepository;

    private final ApplicationEventPublisher eventPublisher;
    private Logger logger = LoggerFactory.getLogger(UserService.class);

    @EventListener(condition = "#authentication ne null and #authentication.principal.id == #newCommentEvent.source.post.author.id")
    public void handleNewComment(NewCommentEvent newCommentEvent) {
        Comment newComment = (Comment) newCommentEvent.getSource();
        Notification notification = new Notification(newComment.getAuthor(), TimeProvider.getTime(),
                String.format("%s %s commented on your post", newComment.getAuthor().getFirstName(), newComment.getAuthor().getLastName()));
        notificationRepository.save(notification);
    }

    public Page<Notification> getNotifications(boolean filterUnread, Pageable pageable) {
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(filterUnread)
            return notificationRepository.findNotificationsByReceiverAndReadTimeIsNullOrderBySendTimeDesc(loggedUser, pageable);
        else
            return notificationRepository.findNotificationsByReceiverOrderBySendTimeDesc(loggedUser, pageable);
    }

    public Page<Post> generateTimeline(TimelineGenerator.TimelineThreshold timelineThreshold, int page, int size) {
        Instant threshold = TimeProvider.getTime().minus(timelineThreshold.getTimeAmount());
        return timelineGenerator.generate(threshold, page, size);
    }

    @EventListener(condition = "event.source.groupId ne null and " +
            "groupRepository.findById(event.source.groupId).isPresent() and " +
            "groupRepository.findById(event.source.groupId).get().users.contains(#authentication.principal)"
    )
    public void handleNewPostInGroup(PostCreatedEvent event) {
        Post post = (Post)event.getSource();
        Notification notification = new Notification(post.getAuthor(), TimeProvider.getTime(),
                String.format("%s %s added new post in %s", post.getAuthor().getFirstName(), post.getAuthor().getLastName(), post.getGroup()));
        notificationRepository.save(notification);
    }

    public Page<User> getFriends(Integer id, Pageable pageable) {
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!loggedUser.getId().equals(id))
            // FIXME: throw
            return Page.empty();
        return userRepository.findAllFriends(loggedUser, pageable);
    }

    public void sendFriendRequest(Integer senderId, Integer targetId) {
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<User> target = userRepository.findById(targetId);

        if(!loggedUser.getId().equals(senderId) || target.isEmpty())
            // FIXME: throw error
            return;

        Friendship friendship = new Friendship(loggedUser, target.get(), TimeProvider.getTime(), "P"); // Pending
        friendshipRepository.save(friendship);
        eventPublisher.publishEvent(new FriendRequestSentEvent(friendship));
    }

    @EventListener
    public void handleFriendRequest(FriendRequestSentEvent event) {
        Friendship friendship = (Friendship) event.getSource();
        User sender = friendship.getSender();
        notificationRepository.save(new Notification(friendship.getReceiver(), TimeProvider.getTime(),
                String.format("%s %s wants to be your friend.%n", sender.getFirstName(), sender.getLastName())));
    }

    public void friendRequestResponseHandler(Integer friendshipId, String answer) {
        Optional<Friendship> friendship = friendshipRepository.findById(friendshipId);
        Friendship.FriendshipStatus status = Friendship.FriendshipStatus.fromString(answer);

        friendship.filter(f -> status != Friendship.FriendshipStatus.PENDING).ifPresent(f -> {
            f.setStatus(answer);
            friendshipRepository.save(f);
            ApplicationEvent event = status.makeEvent(f);
            eventPublisher.publishEvent(event);
        });
    }

    @EventListener
    public void friendRequestRejectedHandler(FriendRequestRejectedEvent event) {
        Friendship friendship = (Friendship) event.getSource();
        User receiver = friendship.getReceiver();
        notificationRepository.save(new Notification(friendship.getSender(), TimeProvider.getTime(),
                String.format("%s %s rejected your friend request.", receiver.getFirstName(), receiver.getLastName())));
    }

    @EventListener
    public void friendRequestAcceptedHandler(FriendRequestAcceptedEvent event) {
        Friendship friendship = (Friendship) event.getSource();
        User receiver = friendship.getReceiver();
        notificationRepository.save(new Notification(friendship.getSender(), TimeProvider.getTime(),
                String.format("You and %s %s are now friends.", receiver.getFirstName(), receiver.getLastName())));
    }
}
