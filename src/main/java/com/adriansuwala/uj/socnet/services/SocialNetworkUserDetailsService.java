package com.adriansuwala.uj.socnet.services;

import com.adriansuwala.uj.socnet.data.UserDto;
import com.adriansuwala.uj.socnet.entities.User;
import com.adriansuwala.uj.socnet.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class SocialNetworkUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    private final Logger logger = LoggerFactory.getLogger(SocialNetworkUserDetailsService.class);

    public SocialNetworkUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(UserDto userDto) {
        logger.debug(userDto.getPassword());
        User user = User.builder()
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .email(userDto.getEmail())
                .password(new BCryptPasswordEncoder().encode(userDto.getPassword()))
                .registrationTime(Instant.now())
                .build();
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        logger.debug(email);
        User user = userRepository.findByEmail(email);
        user.setAuthorities(List.of(new SimpleGrantedAuthority("USER")));
        return user;
    }
}
