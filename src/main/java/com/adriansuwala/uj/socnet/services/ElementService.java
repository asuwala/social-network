package com.adriansuwala.uj.socnet.services;

import com.adriansuwala.uj.socnet.data.CommentDto;
import com.adriansuwala.uj.socnet.data.PostDto;
import com.adriansuwala.uj.socnet.data.events.NewCommentEvent;
import com.adriansuwala.uj.socnet.data.events.PostCreatedEvent;
import com.adriansuwala.uj.socnet.entities.*;
import com.adriansuwala.uj.socnet.repositories.*;
import com.adriansuwala.uj.socnet.util.TimeProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ElementService {
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final ApplicationEventPublisher eventPublisher;
    private final GroupRepository groupRepository;
    private final ReactionRepository reactionRepository;

    public Optional<Post> addPost(PostDto postDto) {
        Optional<User> author = userRepository.findById(postDto.getAuthorId());

        if(!canAddPost(postDto.getAuthorId(), author))
            return Optional.empty();

        Optional<Group> group = groupRepository.findById(postDto.getGroupId());

        Post post = new Post(postDto.getBody(), author.get(), postDto.getVisibility(), group.orElse(null), TimeProvider.getTime());
        postRepository.save(post);
        eventPublisher.publishEvent(new PostCreatedEvent(post));
        return Optional.of(post);
    }

    public Optional<Comment> addComment(CommentDto commentDto) {
        Optional<User> commentAuthor = userRepository.findById(commentDto.getAuthorId());
        Optional<Post> post = postRepository.findById(commentDto.getPostId());
        Optional<Comment> parentComment = commentDto.getParentCommentId() == null ?
                Optional.empty() : commentRepository.findById(commentDto.getParentCommentId());
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(!canAddComment(commentDto.getParentCommentId(), parentComment, commentAuthor))
            return Optional.empty();

        Comment comment = new Comment(commentDto.getBody(), loggedUser, commentDto.getCreationTime(), parentComment.orElse(null), post.get());
        commentRepository.save(comment);
        eventPublisher.publishEvent(new NewCommentEvent(comment));
        return Optional.of(comment);
    }

    public void addReaction(Integer postId) {
        Optional<Post> post = postRepository.findById(postId);
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(!canAddReaction(post))
            // FIXME: throw
            return;
        reactionRepository.save(Reaction.from(loggedUser.getId(), post.get().getId()));
    }

    private boolean canAddReaction(Optional<Post> post) {
        return post.isPresent();
    }

    private boolean canAddComment(Integer parentCommentId, Optional<Comment> parentComment, Optional<User> commentAuthor) {
        return (parentCommentId == null || parentComment.isPresent()) && commentAuthor.isPresent();
    }

    private boolean canAddPost(Integer dtoAuthorId, Optional<User> repoAuthor) {
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return loggedUser.getId().equals(dtoAuthorId) && repoAuthor.isPresent();
    }
}
