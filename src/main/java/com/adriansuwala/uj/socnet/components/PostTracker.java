package com.adriansuwala.uj.socnet.components;

import com.adriansuwala.uj.socnet.data.PostTrackerData;
import com.adriansuwala.uj.socnet.data.events.PostCreatedEvent;
import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.repositories.PostEngagementDataRepository;
import com.adriansuwala.uj.socnet.util.TimeProvider;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class PostTracker {
    private Set<Post> trackedPosts = new HashSet<>();
    private final PostEngagementDataRepository engagementDataRepository;
    private final int interval = 3600 * 1000 * 2;
    private final float REACTION_THRESHOLD = 1.5f;
    private final float COMMENT_THRESHOLD = 1.5f;
    private final Logger logger = LoggerFactory.getLogger(PostTracker.class);

    @Scheduled(fixedRate = interval)
    private void countInterval() {
        logger.info("Gathering post data...");
        for(Post post : trackedPosts) {
            List<PostTrackerData> list = engagementDataRepository.findAllByPostId(post.getId());
            PostTrackerData lastPoint = list.get(list.size() - 1);
            PostTrackerData beforeLastPoint = list.get(list.size() - 2);
            if(lastPoint.reactionCount < REACTION_THRESHOLD * beforeLastPoint.reactionCount
                    || lastPoint.commentCount < COMMENT_THRESHOLD * beforeLastPoint.commentCount)
                trackedPosts.remove(post);
        }
    }

    public Set<Post> getTrackedPosts() {
        return trackedPosts;
    }

    private void addPoint(Post post) {
        PostTrackerData newPoint = new PostTrackerData(post.getId(), post.getReactionCount(), post.getComments().size(), Duration.between(post.getCreationTime(), TimeProvider.getTime()));
        engagementDataRepository.save(newPoint);
    }

    @EventListener
    public void track(PostCreatedEvent event) {
        Post post = (Post) event.getSource();
        addPoint(post);
    }
}
