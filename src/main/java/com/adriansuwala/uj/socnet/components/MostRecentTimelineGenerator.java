package com.adriansuwala.uj.socnet.components;

import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.entities.User;
import com.adriansuwala.uj.socnet.repositories.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
@RequiredArgsConstructor
public class MostRecentTimelineGenerator implements TimelineGenerator {

    private final PostRepository postRepository;

    @Override
    public Page<Post> generate(Instant threshold, int page, int size) {
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return postRepository.findAllVisiblePostsAfter(loggedUser, threshold, PageRequest.of(page, size));
    }
}
