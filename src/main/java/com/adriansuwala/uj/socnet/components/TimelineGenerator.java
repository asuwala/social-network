package com.adriansuwala.uj.socnet.components;

import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.util.TimeProvider;
import org.springframework.data.domain.Page;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalAmount;

public interface TimelineGenerator {
    enum TimelineThreshold {
        DAY {
            @Override
            public TemporalAmount getTimeAmount() {
                return Duration.ofHours(24);
            }
        },
        WEEK {
            @Override
            public TemporalAmount getTimeAmount() {
                return Duration.ofDays(7);
            }
        },
        MONTH {
            @Override
            public TemporalAmount getTimeAmount() {
                return Duration.ofDays(30);
            }
        },
        ALL {
            @Override
            public TemporalAmount getTimeAmount() {
                return Duration.between(Instant.EPOCH, TimeProvider.getTime());
            }
        };

        public abstract TemporalAmount getTimeAmount();
    }

    Page<Post> generate(Instant instant, int page, int size);
}
