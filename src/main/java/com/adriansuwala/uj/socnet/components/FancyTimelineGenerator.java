package com.adriansuwala.uj.socnet.components;

import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.repositories.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
@RequiredArgsConstructor
public class FancyTimelineGenerator implements TimelineGenerator {

    private final PostRepository postRepository;

    @Override
    public Page<Post> generate(Instant instant, int page, int size) {
        // temporary
        return postRepository.findAll(PageRequest.of(page, size));
    }
}
