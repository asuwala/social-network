package com.adriansuwala.uj.socnet.controllers;

import com.adriansuwala.uj.socnet.components.TimelineGenerator;
import com.adriansuwala.uj.socnet.entities.Notification;
import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.entities.User;
import com.adriansuwala.uj.socnet.services.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping(path = "/api/v1")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping(value = "notifications")
    public Page<Notification> getNotifications(@RequestParam("unread") Optional<Boolean> onlyUnread, Pageable pageable) {
        return userService.getNotifications(onlyUnread.orElse(false), pageable);
    }

    @GetMapping(value = "timeline", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Post>> generateTimeline(@RequestParam("t") Optional<String> threshold,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size) {
        TimelineGenerator.TimelineThreshold timelineThreshold =
                TimelineGenerator.TimelineThreshold.valueOf(threshold.orElse("DAY").toUpperCase());

        Page<Post> timeline = userService.generateTimeline(timelineThreshold, page.orElse(0), size.orElse(20));
        return ResponseEntity.ok(timeline);
    }

    @GetMapping(value = "users/{id}/friends")
    public ResponseEntity<Page<User>> getFriends(@PathVariable("id") Integer id, Optional<Integer> page, Optional<Integer> size) {
        Page<User> friendlist = userService.getFriends(id, PageRequest.of(page.orElse(0), size.orElse(20)));
        return ResponseEntity.ok(friendlist);
    }

    @PostMapping(value = "users/{id}/friends")
    public ResponseEntity<Void> sendFriendRequest(@PathVariable("id") Integer id, @RequestParam("targetId") Integer targetId) {
        userService.sendFriendRequest(id, targetId);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "requests/friends/{fid}")
    public ResponseEntity<Void> friendRequestResponseHandler(@PathVariable("fid") Integer friendshipId,
                                                             @RequestParam("answer") String answer) {
        userService.friendRequestResponseHandler(friendshipId, answer);
        return ResponseEntity.ok().build();
    }
}
