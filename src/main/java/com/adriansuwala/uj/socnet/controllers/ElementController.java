package com.adriansuwala.uj.socnet.controllers;

import com.adriansuwala.uj.socnet.data.CommentDto;
import com.adriansuwala.uj.socnet.data.PostDto;
import com.adriansuwala.uj.socnet.entities.Comment;
import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.entities.User;
import com.adriansuwala.uj.socnet.repositories.CommentRepository;
import com.adriansuwala.uj.socnet.repositories.PostRepository;
import com.adriansuwala.uj.socnet.services.ElementService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URI;
import java.util.Optional;

@BasePathAwareController
@RequiredArgsConstructor
public class ElementController {
    private final ElementService elementService;
    private final Logger logger = LoggerFactory.getLogger(ElementController.class);

    @PostMapping(value = "posts", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<URI> addPost(@RequestBody PostDto postDto) {
        logger.debug(postDto.toString());
        Optional<Post> addedPost = elementService.addPost(postDto);
        if(addedPost.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        URI link = WebMvcLinkBuilder.linkTo(PostRepository.class, addedPost).toUri();
        return ResponseEntity.created(link).build();
    }

    @PostMapping(value = "comments/{id}/comments", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<URI> addComment(@PathVariable("id") Integer postId, @RequestBody CommentDto commentDto) {
        logger.debug(commentDto.toString());
        commentDto.setPostId(postId);
        if(commentDto.getAuthorId() == null) {
            User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            commentDto.setAuthorId(loggedUser.getId());
        }
        Optional<Comment> addedComment = elementService.addComment(commentDto);
        if(addedComment.isEmpty())
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        URI link = WebMvcLinkBuilder.linkTo(CommentRepository.class, addedComment).toUri();
        return ResponseEntity.created(link).build();
    }

    @PostMapping(value = "posts/{id}/reactions")
    public @ResponseBody ResponseEntity<Void> addReaction(@PathVariable("id") Integer postId) {
        elementService.addReaction(postId);
        return ResponseEntity.ok().build();
    }
}
