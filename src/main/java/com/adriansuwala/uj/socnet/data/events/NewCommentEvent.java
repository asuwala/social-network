package com.adriansuwala.uj.socnet.data.events;

import com.adriansuwala.uj.socnet.entities.Comment;
import org.springframework.context.ApplicationEvent;

public class NewCommentEvent extends ApplicationEvent {
    public NewCommentEvent(Comment comment) {
        super(comment);
    }
}
