package com.adriansuwala.uj.socnet.data.events;

import com.adriansuwala.uj.socnet.entities.Friendship;
import org.springframework.context.ApplicationEvent;

public class FriendRequestRejectedEvent extends ApplicationEvent {
    public FriendRequestRejectedEvent(Friendship friendship) {
        super(friendship);
    }
}
