package com.adriansuwala.uj.socnet.data;

import lombok.Data;

import java.time.Instant;

@Data
abstract class ElementDto {
    private Instant creationTime;
    private String body;
    private Integer authorId;
}
