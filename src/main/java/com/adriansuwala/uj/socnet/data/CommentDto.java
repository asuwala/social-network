package com.adriansuwala.uj.socnet.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CommentDto extends ElementDto {
    private Integer parentCommentId;
    private Integer postId;
}
