package com.adriansuwala.uj.socnet.data;

import lombok.RequiredArgsConstructor;
import org.springframework.data.keyvalue.annotation.KeySpace;

import javax.persistence.Id;
import java.time.Duration;

@KeySpace
@RequiredArgsConstructor
public class PostTrackerData {
    @Id
    public Integer id;

    public final int postId;
    public final int reactionCount;
    public final int commentCount;
    public final Duration timeSincePosting;
}
