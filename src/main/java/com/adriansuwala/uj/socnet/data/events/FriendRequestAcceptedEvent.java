package com.adriansuwala.uj.socnet.data.events;

import com.adriansuwala.uj.socnet.entities.Friendship;
import org.springframework.context.ApplicationEvent;

public class FriendRequestAcceptedEvent extends ApplicationEvent {
    public FriendRequestAcceptedEvent(Friendship friendship) {
        super(friendship);
    }
}
