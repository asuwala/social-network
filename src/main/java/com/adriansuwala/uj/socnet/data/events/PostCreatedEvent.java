package com.adriansuwala.uj.socnet.data.events;

import com.adriansuwala.uj.socnet.entities.Post;
import org.springframework.context.ApplicationEvent;

public class PostCreatedEvent extends ApplicationEvent {
    public PostCreatedEvent(Post post) {
        super(post);
    }
}
