package com.adriansuwala.uj.socnet.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PostDto extends ElementDto {
    private Integer id;
    private String visibility;
    private Integer groupId;
    private String tags;
}
