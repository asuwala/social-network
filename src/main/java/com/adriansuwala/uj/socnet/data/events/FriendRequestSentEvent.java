package com.adriansuwala.uj.socnet.data.events;

import com.adriansuwala.uj.socnet.entities.Friendship;
import org.springframework.context.ApplicationEvent;

public class FriendRequestSentEvent extends ApplicationEvent {
    public FriendRequestSentEvent(Friendship source) {
        super(source);
    }
}
