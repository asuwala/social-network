package com.adriansuwala.uj.socnet.data;

import lombok.Data;

@Data
public class UserDto {
    private String email;
    private String password;
    private String firstName;
    private String lastName;
}
