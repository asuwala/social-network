package com.adriansuwala.uj.socnet;

import com.adriansuwala.uj.socnet.entities.Post;
import com.adriansuwala.uj.socnet.entities.User;
import com.adriansuwala.uj.socnet.repositories.PostRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.util.Pair;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
// For some reason this doesn't exist?
//import static org.springframework.test.web.servlet.request.RequestPostProcessor.httpBasic;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class SocnetApplicationTests {
    @MockBean(answer = Answers.RETURNS_MOCKS, classes = PostRepository.class, name = "PostRepository")
    private PostRepository postRepository;

    private Post newPost = buildPost(5, "TESTING");
    private List<Post> posts = List.of(buildPost(1, "asdf"), buildPost(2, "qqqqqq"), buildPost(3, "qwer1234"));
    private String newPostAsString = null;
    private final Pair<String, String> header = Pair.of("Authorization", "Basic dGVzdEB0ZXN0LnRlc3Q6dGVzdA==");

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setup() throws JsonProcessingException {
        newPostAsString = objectMapper
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .writer().writeValueAsString(newPost).replace("\"id\": null,", "");
    }

    @Test
    void contextLoads(@Autowired MockMvc mockMvc) {
        assertThat(mockMvc).isNotNull();
    }

    private String getPostsEndpoint() {
        // FIXME: For some reason throws IllegalStateException
        //repositoryEntityLinks.linkToCollectionResource(PostRepository.class).toString();
        return "/api/v1/posts";
    }

    @Test
    void getPosts_Unauthenticated_Successful(@Autowired MockMvc mockMvc) throws Exception {
        when(postRepository.findAll()).thenReturn(posts);
        mockMvc.perform(get(getPostsEndpoint()))
                .andDo(print())
                .andExpect(status().isOk())
                // TODO: Is it possible to compare with this.posts?
                .andExpect(jsonPath("$._embedded.posts").isArray());
    }


    @Test
    void addPost_Authenticated_Succeeds(@Autowired MockMvc mockMvc) throws Exception {
        when(postRepository.save(newPost)).thenReturn(newPost);
        mockMvc.perform(post(getPostsEndpoint()).header(header.getFirst(), header.getSecond())
                .content(newPostAsString))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void addPost_Unauthenticated_Fails(@Autowired MockMvc mockMvc) throws Exception {
        when(postRepository.save(newPost)).thenReturn(newPost);
        mockMvc.perform(post(getPostsEndpoint())
                .content(newPostAsString))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    void addPost_AsOtherThanLoggedIn_Fails(@Autowired MockMvc mockMvc) throws Exception {
        when(postRepository.save(newPost)).thenReturn(newPost);
        mockMvc.perform(post(getPostsEndpoint()).header(header.getFirst(), header.getSecond())
                .content(newPostAsString))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    private static Post buildPost(Integer id, String body) {
        Post p = new Post();
        p.setId(id);
        p.setBody(body);
        p.setCreationTime(Instant.now());
        p.setElementType("POST");
        p.setAuthor(User.builder().id(15).email("test@test.test").password("test").build());
        return p;
    }
}
